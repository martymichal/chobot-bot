use std::collections::BTreeMap;
use std::env;
use std::sync::Arc;

use serenity::async_trait;
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{CommandResult, StandardFramework};
use serenity::model::channel::{Message, ReactionType};
use serenity::prelude::*;
use serenity::utils::MessageBuilder;

use time::Date;

use tokio::sync::RwLock;

// How the member attended the choir practice
enum Attendance {
    Yes,
    No,
    Late,
}

// Whether the member notified their lateness or non-presence on time.
enum Notification {
    OnTime,
    Late,
}

// Info on member's attendance at a choir practice
struct MemberAttendance {
    member: String,
    attendance: Attendance,
    notification: Notification,
}

struct Events;

impl TypeMapKey for Events {
    type Value = Arc<RwLock<BTreeMap<Date, Vec<MemberAttendance>>>>;
}

#[group]
#[commands(dochazka, test)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[tokio::main]
async fn main() {
    // Login with a bot token from the environment
    let token = env::var("DISCORD_TOKEN").expect("token");

    // Set the bot's prefix to "chobot"
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("chobot "))
        .group(&GENERAL_GROUP);

    let intents = GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(token, intents)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    {
        // Open the data lock in write mode, so keys can be inserted to it.
        let mut data = client.data.write().await;

        data.insert::<Events>(Arc::new(RwLock::new(BTreeMap::default())));
    }

    // TODO: Deserialize serialized bot data

    // Start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn dochazka(ctx: &Context, msg: &Message) -> CommandResult {
    let mut builder = MessageBuilder::new();

    let data = ctx.data.write().await;
    let events_lock = data
        .get::<Events>()
        .expect("could not get events from context data")
        .clone();
    let mut events = events_lock.write().await;

    // TODO: Recurrent action scheduling a new choir practice
    /*
    events.insert(
        Date::from_calendar_date(2019, Month::December, 31).unwrap(),
        vec![MemberAttendance {
            member: "x".to_string(),
            attendance: Attendance::Yes,
            notification: Notification::OnTime,
        }],
    );*/

    let mut table = String::new();
    for (date, _attendances) in events.iter() {
        // TODO: Members in columns, dates in rows. Print per line in an ASCII table
        table.push_str(&date.to_string());
        table.push_str("\n");
    }
    builder.push_codeblock(table, None);

    builder.build();

    let res = msg.channel_id.say(&ctx.http, &builder).await;
    let _ = match res {
        Err(SerenityError::Http(_)) => {
            println!("Error sending message");
        }
        Err(_) => {
            if let Err(err) = msg
                .channel_id
                .say(
                    &ctx.http,
                    "Záznam je příliš dlouhý. Bot potřebuje přeprogramovat.",
                )
                .await
            {
                println!("Error sending message: {:?}", err);
            }
        }
        _ => (),
    };

    Ok(())
}

#[command]
async fn test(ctx: &Context, msg: &Message) -> CommandResult {
    let reactions = vec![
        ReactionType::Unicode("\u{274C}".to_string()), // ❌️
        ReactionType::Unicode("\u{23F0}".to_string()), // ⏰️
    ];

    if let Err(err) = msg
        .channel_id
        .send_message(&ctx.http, |m| m.content("TEST").reactions(reactions))
        .await
    {
        println!("Error sending message: {:?}", err);
    }

    Ok(())
}
